import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SwapiGrabberService } from '../swapi-grabber.service';

@Component({
  selector: 'app-select-form',
  templateUrl: './select-form.component.html',
  styleUrls: ['./select-form.component.css']
})
export class SelectFormComponent implements OnInit {

  categories= ['people','vehicles','species','starships','planets']
  selectedNumber
  selectedCategory
  @Output() selectedObject = new EventEmitter<[string,number]>()
  swapidata
  
  constructor() { }

  ngOnInit() {
  }
  
  getData_form() {
   // this.swapigrabber.getData(this.selectedCategory,this.selectedNumber).subscribe( (data)=>{this.swapidata = data})
    
   this.selectedObject.emit([this.selectedCategory,this.selectedNumber])
  }

}
