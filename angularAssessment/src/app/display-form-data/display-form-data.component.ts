import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../person';
import { Planet } from '../planet';
import { Species } from '../species';
import { Starship } from '../starship';
import { Vehicle } from '../vehicle';

@Component({
  selector: 'app-display-form-data',
  templateUrl: './display-form-data.component.html',
  styleUrls: ['./display-form-data.component.css']
})
export class DisplayFormDataComponent implements OnInit {

  @Input() dataObject
  @Input() category


  constructor() { }

  ngOnInit() {

  }


}
