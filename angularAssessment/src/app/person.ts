import { Planet } from './planet';
import { Species } from './species';
import { Vehicle } from './vehicle';
import { Starship } from './starship';

export class Person {
    name:string
    height:number
    mass:number
    hair_color:string
    skin_color:string
    eye_color:string
    birth_year:string
    gender:string
    homeworld:Planet
    films:string[]
    species:Species
    vehicles:Vehicle[]
    starships:Starship[]
    created:string
    edited:string
    url:string
}
