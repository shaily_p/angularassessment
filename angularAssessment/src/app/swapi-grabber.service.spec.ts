import { TestBed } from '@angular/core/testing';

import { SwapiGrabberService } from './swapi-grabber.service';

describe('SwapiGrabberService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SwapiGrabberService = TestBed.get(SwapiGrabberService);
    expect(service).toBeTruthy();
  });
});
