import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { SelectFormComponent } from './select-form/select-form.component';
import { DisplayFormDataComponent } from './display-form-data/display-form-data.component'

@NgModule({
  declarations: [
    AppComponent,
    SelectFormComponent,
    DisplayFormDataComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
