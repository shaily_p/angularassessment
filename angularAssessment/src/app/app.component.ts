import { Component } from '@angular/core';
import { SwapiGrabberService } from './swapi-grabber.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent {
  title = 'angularAssessment';  
  selectedObjectForm:[string,number]
  selectedCategory
  selectedNumber
  swapidata = []
  constructor(private swapigrabber :SwapiGrabberService){
  
  }

  getDataFromtheService(){
     this.swapigrabber.getData(this.selectedObjectForm[0],this.selectedObjectForm[1]).subscribe((data)=>{this.swapidata = data})
  }
  passingData($event){
    this.selectedObjectForm = $event
    this.selectedCategory = this.selectedObjectForm[0]
    this.selectedNumber = this.selectedObjectForm[1]
    console.log("Recieved event")
    this.getDataFromtheService()

  } 
}
