import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators'
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SwapiGrabberService {
  swapiUrl:string = 'https://swapi.co/api'
  personUrl:string = 'people'
  planetUrl:string = 'planets'
  speciesUrl:string = 'species'
  starshipUrl:string = 'starships'
  vehicleUrl:string = 'vehicles'
  constructor(private http:HttpClient) { }

  private handleError(category = 'category', ID,  result? : any) {
    return (error: any): Observable<any> => {
      console.log(`failed to get ${category} with ID ${ID}`); 
      return of (result as any);
    }
  }

  getData(category:string, ID:number):Observable<any>  {
    return this.http.get(`${this.swapiUrl}/${category}/${ID}`).pipe(
      catchError(this.handleError(category, ID, []))
    )
  }

 

  /** 
  getPerson(ID:number) {
    return this.http.get(`${this.swapiUrl}/${this.personUrl}/${ID}`)
  }

  getPlanet(ID:number) {
    return this.http.get(`${this.swapiUrl}/${this.personUrl}/${ID}`)
  }

  getSpecies(ID:number) {
    return this.http.get(`${this.swapiUrl}/${this.personUrl}/${ID}`)
  }

  getStarship(ID:number) {
    return this.http.get(`${this.swapiUrl}/${this.personUrl}/${ID}`)
  }

  getVehicle(ID:number) {
    return this.http.get(`${this.swapiUrl}/${this.personUrl}/${ID}`)
  } **/

}
