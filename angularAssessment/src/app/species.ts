import { Planet } from './planet';
import { Person } from './person';

export class Species {
    name:string
    classification:string
    designation:string
    average_height:number
    skin_colors:string
    eye_colors:string
    average_lifespan:number
    homeworld:Planet
    language:string
    people:Person[]
    films:string[]
    created:string
    edited:string
    url:string
}
